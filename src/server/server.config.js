// const fs = require('fs');
// const https = require('https');

// const tls = {
//   key: fs.readFileSync('key.pem'),
//   cert: fs.readFileSync('cert.pem'),
//   passphrase: 'palash' || process.env.passphrase,
// };

const serverConfig = {
  host: '0.0.0.0' || process.env.host,
  port: 80 || process.env.port,
  // tls,
};

module.exports = serverConfig;
