import users from './strategies/user';

const register = async (server, options) => {
  // All the server strategy will be registered here
  // user strategy is registered here as only user will only access it
  server.auth.strategy('user', 'jwt', users);

  server.auth.default('user');
};

export default register;
