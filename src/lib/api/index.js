import userApi from './users/index';
import linkApi from './links/index';

const routes = [...userApi, ...linkApi];

export default routes;
