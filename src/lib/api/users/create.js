import joi from 'joi';

import { createUser } from '../../database/mongodb/functions/users';

export default {
  description:
    'This is the api used to create the user with name,username,email' +
    'and password',
  notes: 'Make sure auth is for user only',
  tags: ['api', 'user'],
  validate: {
    payload: {
      username: joi.string().required(),
      name: joi.string().required(),
      password: joi.string().required(),
      email: joi.string().required(),
    },
  },
  async handler(request, h) {
    try {
      /*eslint-disable */
      const { username, name, password, email } = request.payload;
      /* eslint-enable */

      const res = await createUser({
        username,
        name,
        password,
        email,
      });

      return h.response(res);
    } catch (error) {
      console.log(error);

      return {
        statusCode: 500,
        error: 'Server Error',
      };
    }
  },
};
