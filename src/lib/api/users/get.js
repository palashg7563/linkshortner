import joi from 'joi';

import { getUserById } from '../../database/mongodb/functions/users';

export default {
  description: 'This API will get the user',
  notes: 'In auth user must be the user',
  tags: ['api', 'user'],
  validate: {
    params: {
      id: joi.string().required(),
    },
  },
  async handler(request, h) {
    try {
      const { id } = request.params;

      const res = await getUserById({ id });

      return h.response(res);
    } catch (error) {
      console.log(error);

      return {
        statusCode: 500,
        error: 'Server Error',
      };
    }
  },
};
