#use the base image node 8.9.0
FROM node:8.9.0

#the working directory of the project will be /shortner
WORKDIR /shortner

EXPOSE 80

#add the package.json
ADD package.json /shortner
ADD yarn.lock /shortner

#run yarn install
RUN yarn install

#after that add all the contents to the working directory
ADD . /shortner